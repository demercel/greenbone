from sqlalchemy import create_engine, MetaData, Table, Column, String

from flask import Flask, request
from flask_restful import Resource, Api
from flask import jsonify

import requests


dbConnection = create_engine('sqlite:///computers.db', echo = True)
meta = MetaData()

API_ENDPOINT = 'http://172.17.0.2:8080/api/notify'
levels = ["warning", "info", "error"]

computers = Table(
	'computers', meta, 
	Column('MAC', String, primary_key = True), 
	Column('name', String, nullable=False), 
	Column('IP', String, nullable=False),
	Column('employee', String, nullable=False),
	Column('description', String, nullable=True),	
)

# ~ meta.create_all(dbConnection)

# ~ conn = dbConnection.connect()
# ~ ins = computers.insert().values(MAC = '11:dd:b1:b9:ab:8c', name = 'Server 02', IP = '192.168.0.2', employee = 'MMA', description = 'missing')
# ~ ins = computers.insert().values(MAC = '11:dd:b1:b9:ac:8c', name = 'Mail server', IP = '192.168.0.3', employee = 'MMA', description = 'missing')
# ~ result = conn.execute(ins)


	
app = Flask(__name__)
api = Api(app)

# ~ DONE The system administrator wants to be able to add a new computer to an employee
# ~ DONE The system administrator wants to be informed when an employee is assigned 3 or more computers.
# ~ DONE The system administrator wants to be able to get all assigned computers for an employee
# ~ DONE The system administrator wants to be able to remove a computer from an employee
# ~ DONE The system administrator wants to be able to assign a computer to another employee

# ~ Assumptions: 
# ~ one computer can be assigned to multiple employees
# ~ remove computer is done with a json request

class Computers_Employee(Resource):
	def get(self, employee_id):
		conn = dbConnection.connect()
		s = computers.select().where(computers.c.employee == employee_id)
		row_result = conn.execute(s)
		result = {'data': [dict(zip(tuple (computers.columns.keys()) ,row)) for row in row_result]}
		return jsonify(result)
	

class New_Computer(Resource):	
	def post(self):
		conn = dbConnection.connect()
		# ~ print(request.json)
		ins = computers.insert().values(MAC = request.json['MAC'], name = request.json['name'], IP = request.json['IP'], employee = request.json['employee'], description = request.json['description'])
		result = conn.execute(ins)
		verify43(conn, request.json['employee'])
		return '', 201


class Remove_Computer(Resource):	
	def post(self):
		conn = dbConnection.connect()
		user = request.json['employee']#request.args.get('user', '')
		computer = request.json['MAC']#request.args.get('MAC', '')
		stmt = computers.delete().where(computers.c.MAC == computer and computers.c.employee == user)
		result = conn.execute(stmt)
		return '', 204


class Assign_Computer(Resource):	
	def post(self):
		conn = dbConnection.connect()
		user = request.json['employee']
		computer = request.json['MAC']
		upd = computers.update().where(computers.c.MAC == computer).values(employee = user)
		result = conn.execute(upd)
		verify43(conn, user)
		return '', 201


def verify43(conn, employee):
	print ("verify43")
	if getComputersForEmployee(employee, conn)>=3:
		postMessage('warning', employee, "More than 3 computers")
	

def getComputersForEmployee(employee, connection):
	s = computers.select(computers.c.MAC).where(computers.c.employee == employee)
	row_result = connection.execute(s)
	counter = 0
	for row in row_result:
		counter+=1
	return counter
	

def postMessage(level, employee, message):
	print ("postMessage")
	if level in levels:
		data = {"level": level,
				"employeeAbbreviation": employee,
				"message": message}
		  
		r = requests.post(url = API_ENDPOINT, json = data)	  
		answer = r.text
		print(answer)
		

api.add_resource(Computers_Employee, '/computers/<employee_id>')
api.add_resource(New_Computer, '/computers')
api.add_resource(Remove_Computer, '/computerdel')
api.add_resource(Assign_Computer, '/computerassign')


if __name__ == '__main__':
	 app.run(port='5002')
